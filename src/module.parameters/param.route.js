/* eslint-disable */
import Parameter from './view.parameter'
import paramComponent from './component/parameter/paramater.component'
import userprofileComponent from './component/parameter/userprofile/userprofile.component'
import baseinfoComponent from './component/parameter/userprofile/tab/baseinfo.component'
import indentifiantComponent from './component/parameter/userprofile/tab/identifiant.component'
import securiteComponent from './component/parameter/userprofile/tab/securite.component'
import entrepriseComponent from './component/parameter/userprofile/tab/entreprise.component'
const routes = [
  {
    path: '/parameter',
    component: Parameter,
    children: [
      {
        path: '',
        component: paramComponent
      },
      {
        path: 'userprofile',
        component: userprofileComponent,
        props: true,
        children: [
          {
            path: 'base',
            name: 'base',
            component: baseinfoComponent
          },
          {
            path: 'identifiant',
            name: 'identifiant',
            component: indentifiantComponent
          },
          {
            path: 'securite',
            name: 'securite',
            component: securiteComponent
          },
          {
            path: 'entreprise',
            name: 'entreprise',
            component: entrepriseComponent
          }
        ]
      }
    ]
  }
]

export default routes
