/* eslint-disable */
const store = {
  namespaced: true,
  state: {
    scroll: 0,
    searchCompany: ''
  },
  mutations: {
    onscroll (state, payload) {
      state.scroll = payload
    },
    searchCompany (state, payload) {
      state.searchCompany = payload
    }
  }
}

export default store
