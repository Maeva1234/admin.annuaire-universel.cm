/* eslint-disable semi */
/* eslint-disable eol-last */
import Vue from 'vue'
import VueRouter from 'vue-router'
import authRoute from '../module.users/module.auth/auth.route'
import paramRoute from '../module.parameters/param.route'
import contactRoute from '../module.contacts/contact.route'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    ...authRoute,
    ...paramRoute,
    ...contactRoute
  ]
})

export default router
