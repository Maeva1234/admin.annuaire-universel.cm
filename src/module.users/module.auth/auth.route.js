/* eslint-disable */
import auth from './view_authentification.vue'
import LoginUsername from './components/authentification/signin/component_signin_username'
import LoginPassword from './components/authentification/signin/component_signin_password'
import SigninPersonnel from './components/authentification/signup/component_signup_personnel_info'

const routes = [
  {
    path: '/',
    name: 'Login',
    redirect: '/login'
  },
  {
    path: '/',
    component: auth,
    children: [
      {
        path: 'login',
        component: LoginUsername
      },
      {
        path: 'login/password',
        component: LoginPassword
      },
      {
        path: 'signup/personnal',
        component: SigninPersonnel
      }
    ]
  }
]

export default routes
