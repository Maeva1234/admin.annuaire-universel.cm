/* eslint-disable */
import Vue from 'vue'
import Vuex from 'vuex'
import authModule from './module.users/module.auth/auth.store'
import paramModule from './module.parameters/param.store'
import contactModule from './module.contacts/contact.store'

Vue.use(Vuex)

export default new Vuex.Store({
  ...authModule,
  ...paramModule,
  ...contactModule
})
