/* eslint-disable */
import Contact from './view.contact'
import importContact from './components/importContact'
import contactList from './components/contactList'
import contactDetail from './components/contactDetail/contactDetail'
const routes = [
  {
    path: '/contact',
    component: Contact,
    children: [
      {
        path: 'list',
        component: contactList,
        children: [
          {
            path:'detail',
            component: contactDetail
          }
        ]
      },
      {
        path: 'import',
        component: importContact
      }
    ]
  }
]
export default routes
