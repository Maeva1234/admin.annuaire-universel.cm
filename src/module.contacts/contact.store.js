/* eslint-disable */
const store = {
  namespaced: true,
  state: {
    detail: false,
    detail1: false,
    scroll: 0,
    searchCompany: '',
    searchSelect: 'Toutes',
    importData: [],
    selectedImport: []
  },
  mutations: {
    onscroll (state, payload) {
      state.scroll = payload
    },
    goDetail (state, payload) {
      state.detail = payload
    },
    goDetail1 (state, payload) {
      state.detail1 = payload
    },
    searchCompany (state, payload) {
      state.searchCompany = payload
    },
    searchSelect (state, payload) {
      state.searchSelect = payload
    },
    importData (state, payload) {
      state.importData = payload
    },
    selectedImport (state, payload) {
      state.selectedImport = payload
    },
  }
}

export default store
